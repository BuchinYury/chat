package sqlstore

import (
	"chat/internal/app/chat/models"
)

// ReceiveMessageTestUserRepository
type ReceiveMessageTestUserRepository struct{}

func (r *ReceiveMessageTestUserRepository) FindByLogin(login string) (*models.User, error) {
	user := &models.User{
		Id:    1,
		Login: login,
	}

	return user, nil
}

func (r *ReceiveMessageTestUserRepository) Create(u *models.User) error {
	return nil
}

// ReceiveMessageTestChatRepository
type ReceiveMessageTestChatRepository struct{}

func (r *ReceiveMessageTestChatRepository) GetUser(chatId int64, userId int64) (*models.User, error) {
	return nil, nil
}

func (r *ReceiveMessageTestChatRepository) AddUser(chatId int64, userToChat *models.User) error {
	return nil
}

func (r *ReceiveMessageTestChatRepository) GetChats(user *models.User) ([]*models.Chat, error) {
	return nil, nil
}

func (r *ReceiveMessageTestChatRepository) GetUsers(chatId int64) ([]*models.User, error) {
	return nil, nil
}

func (r *ReceiveMessageTestChatRepository) GetMessages(chatId int64) ([]*models.Message, error) {
	return nil, nil
}

func (r *ReceiveMessageTestChatRepository) CreateMessage(usersId int64, message *models.Message) error {
	return nil
}

func (r *ReceiveMessageTestChatRepository) CreateChat(users []*models.User) (*models.Chat, error) {
	return nil, nil
}

// LoginTestDataBus
type ReceiveMessageTestDataBus struct{}

func (r *ReceiveMessageTestDataBus) SubscribeToChannel(toChannel string, channel chan []byte) error {
	return nil
}

func (r *ReceiveMessageTestDataBus) PublishToChannel(toChannel string, channel chan []byte) {

}
