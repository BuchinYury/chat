package chatserver

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	BindAddr          string      `json:"bind_addr"`
	LogLevel          string      `json:"log_level"`
	LoginCookieExpire int         `json:"login_cookie_expire"`
	DataBusChannel    string      `json:"data_bus_channel"`
	CommonChatId      int64       `json:"common_chat_id"`
	RedisConfig       RedisConfig `json:"redis"`
	DatabaseUrl       string      `json:"database_url"`
}

type RedisConfig struct {
	Addr     string `json:"addr"`
	Password string `json:"password"`
	Db       int    `json:"db"`
}

func NewConfig() *Config {
	return &Config{}
}

func FromFile(filePath string) (*Config, error) {
	configBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	config := NewConfig()
	err = json.Unmarshal(configBytes, &config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
