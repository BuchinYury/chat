package sqlstore

import (
	"chat/internal/app/chat/models"
	"database/sql"
)

// LoginTestUserRepository
type LoginTestUserRepository struct{}

func (r *LoginTestUserRepository) FindByLogin(login string) (*models.User, error) {
	if login == "good" {
		return &models.User{
				Id:    1,
				Login: "good",
			},
			nil
	}

	return nil, sql.ErrNoRows
}

func (r *LoginTestUserRepository) Create(u *models.User) error {
	return nil
}

// LoginTestChatRepository
type LoginTestChatRepository struct{}

func (r *LoginTestChatRepository) GetUser(chatId int64, userId int64) (*models.User, error) {
	return nil, nil
}

func (r *LoginTestChatRepository) AddUser(chatId int64, userToChat *models.User) error {
	return nil
}

func (r *LoginTestChatRepository) GetChats(user *models.User) ([]*models.Chat, error) {
	return nil, nil
}

func (r *LoginTestChatRepository) GetUsers(chatId int64) ([]*models.User, error) {
	return nil, nil
}

func (r *LoginTestChatRepository) GetMessages(chatId int64) ([]*models.Message, error) {
	return nil, nil
}

func (r *LoginTestChatRepository) CreateMessage(usersId int64, message *models.Message) error {
	return nil
}

func (r *LoginTestChatRepository) CreateChat(users []*models.User) (*models.Chat, error) {
	return nil, nil
}

// LoginTestDataBus
type LoginTestDataBus struct{}

func (r *LoginTestDataBus) SubscribeToChannel(toChannel string, channel chan []byte) error {
	return nil
}

func (r *LoginTestDataBus) PublishToChannel(toChannel string, channel chan []byte) {
	go func() {
		<-channel
	}()
}
