package chatserver

import (
	"chat/internal/logger"
	"context"
	"net/http"
)

const (
	CtxKeyUser ctxKey = iota
)

type ctxKey int8

func StaticFile(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.FileServer(http.Dir("web/static")).ServeHTTP(w, r)
			return
		}

		next(w, r)
	}
}

func AuthenticateUser(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c, err := r.Cookie("user")
		if err != nil {
			logger.GetLogger().Info().Msg("redirect to /login")
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
		next(w, r.WithContext(context.WithValue(r.Context(), CtxKeyUser, c.Value)))
	}
}
