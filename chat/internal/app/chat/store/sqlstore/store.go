package sqlstore

import (
	"chat/internal/app/chat/store"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	db             *sqlx.DB
	UserRepository store.UserRepository
	ChatRepository store.ChatRepository
}

func NewDB(dbURL string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dbURL)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}

func New(db *sqlx.DB) *Store {
	return &Store{
		db: db,
		UserRepository: &UserRepository{
			db: db,
		},
		ChatRepository: &ChatRepository{
			db: db,
		},
	}
}
