package sqlstore

import (
	"chat/internal/app/chat/models"
	"github.com/jmoiron/sqlx"
)

type UserRepository struct {
	db *sqlx.DB
}

func (r *UserRepository) FindByLogin(login string) (*models.User, error) {
	u := &models.User{}
	err := r.db.Get(u, `SELECT id, login FROM users WHERE login = ?`, login)
	if err != nil {
		return nil, err
	}

	return u, nil
}

func (r *UserRepository) Create(u *models.User) error {
	res, err := r.db.Exec(`INSERT INTO users (login) VALUES (?)`, u.Login)
	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	u.Id = id
	return nil
}
