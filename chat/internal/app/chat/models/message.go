package models

import "encoding/json"

// Receive
type ReceiveMessage struct {
	Type string          `json:"type"`
	Data json.RawMessage `json:"data"`
}

type ReceiveNewMessageData struct {
	ChatId int64  `json:"chat_id"`
	Text   string `json:"text"`
}

type ReceiveCreateChat struct {
	Users []*User `json:"users"`
}

// Send
type SendMessage struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type SendChat struct {
	Id       int64      `json:"id"`
	Users    []*User    `json:"users"`
	Messages []*Message `json:"messages"`
}
