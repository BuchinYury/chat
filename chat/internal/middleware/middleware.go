package middleware

import (
	"chat/internal/logger"
	"fmt"
	"net/http"
	"time"
)

type Middleware func(http.HandlerFunc) http.HandlerFunc

func ChainMiddleware(h http.HandlerFunc, m ...Middleware) http.HandlerFunc {
	if len(m) < 1 {
		return h
	}
	wrapped := h
	for i := len(m) - 1; i >= 0; i-- {
		wrapped = m[i](wrapped)
	}
	return wrapped
}

func PanicRecover(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				logger.GetLogger().Info().Msg("panic recovered")
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()

		next(w, r)
	}
}

func Log(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logMessage := fmt.Sprintf("started %s %s", r.Method, r.RequestURI)
		logger.GetLogger().Info().Msg(logMessage)

		start := time.Now()
		rw := &responseWriter{w, http.StatusOK}
		next(rw, r)

		logMessage = fmt.Sprintf("%s %s completed with %d %s in %v",
			r.Method,
			r.RequestURI,
			rw.code,
			http.StatusText(rw.code),
			time.Now().Sub(start),
		)
		switch {
		case rw.code >= 500:
			logger.GetLogger().Error().Msg(logMessage)
		case rw.code >= 400:
			logger.GetLogger().Warn().Msg(logMessage)
		default:
			logger.GetLogger().Info().Msg(logMessage)
		}
	}
}
