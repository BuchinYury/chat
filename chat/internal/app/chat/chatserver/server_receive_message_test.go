package chatserver

import (
	"chat/internal/app/chat/models"
	"chat/internal/app/chat/store/sqlstore"
	testsqlstore "chat/internal/app/chat/test/sqlstore"
	"chat/internal/app/chat/websocket"
	"encoding/json"
	"reflect"
	"testing"
)

func Test_ServerReceiveMessage(t *testing.T) {
	server, err := getReceiveMessageServer()
	if err != nil {
		t.Fatal(err.Error())
	}

	server.runWSHandlers()

	type testCase struct {
		name                string
		message             *websocket.FromClientMessage
		expectedSendMessage models.SendMessage
	}

	testCases := []testCase{
		{
			name:    "new_message",
			message: getReceiveMessageNewMessageType(t, "new_message"),
			expectedSendMessage: models.SendMessage{
				Type: "update_chat",
				Data: models.SendChat{
					Id:    1,
					Users: nil,
					Messages: []*models.Message{
						{
							Id:        1,
							UserLogin: "new_message",
							ChatId:    1,
							Message:   "new_message",
						},
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			go func(tc testCase) {
				server.receive <- tc.message
			}(tc)

			actualSendMessageByte := <-server.toDataBus

			expectedSendMessageByte, err := json.Marshal(tc.expectedSendMessage)
			if err != nil {
				t.Fatal(err.Error())
			}

			if reflect.DeepEqual(expectedSendMessageByte, actualSendMessageByte) {
				t.Fail()
			}
		})
	}
}

// Server
func getReceiveMessageServer() (*chatServer, error) {
	dataBus := &testsqlstore.ReceiveMessageTestDataBus{}

	store := &sqlstore.Store{
		UserRepository: &testsqlstore.ReceiveMessageTestUserRepository{},
		ChatRepository: &testsqlstore.ReceiveMessageTestChatRepository{},
	}

	server, err := New(Config{}, dataBus, store)
	if err != nil {
		return nil, err
	}

	return server, nil
}

// Msg
func getReceiveMessageNewMessageType(t *testing.T, login string) *websocket.FromClientMessage {
	client := &websocket.Client{
		Login: login,
	}

	data := models.ReceiveNewMessageData{
		ChatId: 1,
		Text:   "new_message",
	}
	dataBytes, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err.Error())
	}

	receiveMessage := models.ReceiveMessage{
		Type: "new_message",
		Data: dataBytes,
	}
	msgBytes, err := json.Marshal(receiveMessage)
	if err != nil {
		t.Fatal(err.Error())
	}

	return &websocket.FromClientMessage{
		Client:  client,
		Message: msgBytes,
	}
}
