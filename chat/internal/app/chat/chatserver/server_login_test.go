package chatserver

import (
	"chat/internal/app/chat/store/sqlstore"
	testsqlstore "chat/internal/app/chat/test/sqlstore"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_ServerLogin(t *testing.T) {
	server, err := GetLoginTestServer()
	if err != nil {
		t.Fatal(err.Error())
	}

	testCases := []struct {
		name         string
		method       string
		login        string
		expectedCode int
	}{
		{
			name:         "good_login",
			method:       http.MethodPost,
			login:        "test",
			expectedCode: http.StatusSeeOther,
		},
		{
			name:         "bad_login",
			method:       http.MethodPost,
			login:        "",
			expectedCode: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, "/login", nil)
			req.Form = map[string][]string{"login": {tc.login}}

			server.loginHandler(rec, req)

			if tc.expectedCode != rec.Code {
				t.Fail()
			}
		})
	}
}

// Server
func GetLoginTestServer() (*chatServer, error) {
	dataBus := &testsqlstore.LoginTestDataBus{}

	store := &sqlstore.Store{
		UserRepository: &testsqlstore.LoginTestUserRepository{},
		ChatRepository: &testsqlstore.LoginTestChatRepository{},
	}

	server, err := New(Config{}, dataBus, store)
	if err != nil {
		return nil, err
	}

	return server, nil
}
