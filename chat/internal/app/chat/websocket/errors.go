package websocket

import "errors"

var (
	ErrMethodNotFound = errors.New("method not found")
)
