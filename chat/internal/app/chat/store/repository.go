package store

import (
	"chat/internal/app/chat/models"
)

type UserRepository interface {
	Create(*models.User) error
	FindByLogin(string) (*models.User, error)
}

type ChatRepository interface {
	GetUser(chatId int64, userId int64) (*models.User, error)
	AddUser(chatId int64, userToChat *models.User) error
	GetChats(user *models.User) ([]*models.Chat, error)
	GetUsers(chatId int64) ([]*models.User, error)
	GetMessages(chatId int64) ([]*models.Message, error)
	CreateMessage(usersId int64, message *models.Message) error
	CreateChat(users []*models.User) (*models.Chat, error)
}
