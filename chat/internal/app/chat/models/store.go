package models

type User struct {
	Id    int64  `json:"id"`
	Login string `json:"login"`
}

type Chat struct {
	Id       int64      `json:"id"`
	Users    []*User    `json:"users"`
	Messages []*Message `json:"messages"`
}

type Message struct {
	Id        int64  `json:"id"`
	UserLogin string `json:"user_login" db:"user_login"`
	ChatId    int64  `json:"chat_id" db:"chat_id"`
	Message   string `json:"message"`
}
