package chatserver

import "errors"

var (
	ErrBadLogin = errors.New("bad login")
	ErrLoginNotBeEmpty = errors.New("login not be empty")
)
