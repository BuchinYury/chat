package sqlstore

import (
	"chat/internal/app/chat/models"
	"chat/internal/logger"
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type ChatRepository struct {
	db *sqlx.DB
}

func (r *ChatRepository) GetUser(chatId int64, userId int64) (*models.User, error) {
	user := &models.User{}
	err := r.db.Get(user, `
SELECT users.id, login
FROM users
	JOIN chat_users ON chat_users.user_id = users.id
WHERE chat_id = ?
	AND user_id = ?
LIMIT 1`,
		chatId,
		userId)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *ChatRepository) AddUser(chatId int64, userToChat *models.User) error {
	usersInChat, err := r.GetUser(chatId, userToChat.Id)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if usersInChat == nil {
		_, err := r.db.Exec(`INSERT INTO chat_users (chat_id, user_id) VALUES (?, ?)`, chatId, userToChat.Id)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ChatRepository) GetChats(user *models.User) ([]*models.Chat, error) {
	var chats []*models.Chat
	err := r.db.Select(&chats, `
SELECT chats.id
FROM chats
	JOIN chat_users ON chat_users.chat_id = chats.id
WHERE user_id = ?`,
		user.Id)
	if err != nil {
		return nil, err
	}

	for _, chat := range chats {
		users, err := r.GetUsers(chat.Id)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return nil, err
		}
		chat.Users = users

		messages, err := r.GetMessages(chat.Id)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return nil, err
		}
		chat.Messages = messages
	}

	return chats, nil
}

func (r *ChatRepository) GetUsers(chatId int64) ([]*models.User, error) {
	var users []*models.User
	err := r.db.Select(&users, `
SELECT users.id, users.login
FROM users
	JOIN chat_users ON chat_users.user_id = users.id
WHERE chat_id = ?`,
		chatId)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *ChatRepository) GetMessages(chatId int64) ([]*models.Message, error) {
	var messages []*models.Message
	err := r.db.Select(&messages, `
SELECT messages.id, login AS user_login, chat_id, message
FROM messages
         JOIN chats ON messages.chat_id = chats.id
         JOIN users ON messages.users_id = users.id
WHERE chat_id = ?
ORDER BY messages.id`,
		chatId)
	if err != nil {
		return nil, err
	}

	return messages, nil
}

func (r *ChatRepository) CreateMessage(usersId int64, message *models.Message) error {
	res, err := r.db.Exec(`INSERT INTO messages (users_id, chat_id, message) VALUES (?, ?, ?)`,
		usersId,
		message.ChatId,
		message.Message)
	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	message.Id = id
	return nil
}

func (r *ChatRepository) CreateChat(users []*models.User) (*models.Chat, error) {
	chat := &models.Chat{
		Users: users,
	}

	res, err := r.db.Exec(`INSERT INTO chats (id) values (null)`)
	if err != nil {
		return nil, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	chat.Id = id

	for _, user := range users {
		err = r.AddUser(chat.Id, user)
		if err != nil {
			return chat, err
		}
	}

	return chat, nil
}
