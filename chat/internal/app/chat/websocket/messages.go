package websocket

import (
	"chat/internal/app/chat/models"
	"encoding/json"
)

const (
	MessageMethodGetMessages MessageMethod = iota
	MessageMethodCreateMessage
	MessageMethodCreateChat
	MessageMethodGetChats
)

type MessageMethod int

func (m MessageMethod) String() string {
	intToStrMap := map[MessageMethod]string{
		MessageMethodCreateMessage: "create_message",
		MessageMethodGetMessages:   "get_messages",
		MessageMethodCreateChat:    "create_chat",
		MessageMethodGetChats:          "get_chats",
	}

	if str, ok := intToStrMap[m]; ok {
		return str
	} else {
		return "undefined"
	}
}

type Message struct {
	Method string          `json:"method"`
	Data   json.RawMessage `json:"data"`
}

func (m *Message) GetMethod() (MessageMethod, error) {
	switch m.Method {
	case "create_message":
		return MessageMethodCreateMessage, nil
	case "get_messages":
		return MessageMethodGetMessages, nil
	case "create_chat":
		return MessageMethodCreateChat, nil
	case "get_chats":
		return MessageMethodGetChats, nil
	default:
		return -1, ErrMethodNotFound
	}
}

type MessageDataCreateMessage struct {
	ChatID int    `json:"chat_id,string"`
	Text   string `json:"text"`
}

type MessageDataGetMessages struct {
	ChatID int `json:"chat_id,string"`
}

type MessageDataCreateChat struct {
	Users []string `json:"users"`
}

type MessageDataGetChats struct {
	Chats []*models.Chat `json:"chats"`
}
