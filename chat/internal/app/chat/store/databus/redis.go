package databus

import (
	"chat/internal/logger"
	"github.com/go-redis/redis"
)

type DataBus interface {
	SubscribeToChannel(toChannel string, channel chan []byte) error
	PublishToChannel(toChannel string, channel chan []byte)
}

type Redis struct {
	*redis.Client
}

func New(addr string, password string, db int) (*Redis, error) {
	redisClientOptions := &redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	}

	client := &Redis{Client: redis.NewClient(redisClientOptions)}

	pong, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	logger.GetLogger().Info().Msg("Redis create success")
	logger.GetLogger().Info().Msg("Redis client ping result: " + pong)

	return client, nil
}

func (r *Redis) SubscribeToChannel(toChannel string, channel chan []byte) error {
	pubsub := r.Client.Subscribe(toChannel)
	_, err := pubsub.Receive()
	if err != nil {
		return err
	}

	go func() {
		for msg := range pubsub.Channel() {
			channel <- []byte(msg.Payload)
		}
	}()

	return nil
}

func (r *Redis) PublishToChannel(toChannel string, channel chan []byte) {
	go func() {
		for {
			select {
			case msg := <-channel:
				err := r.Client.Publish(toChannel, msg).Err()
				if err != nil {
					logger.GetLogger().Error().Msg(err.Error())
				}
			}
		}
	}()
}
