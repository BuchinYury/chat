package chatserver

import (
	"chat/internal/app/chat/models"
	"chat/internal/app/chat/websocket"
	"chat/internal/logger"
	"encoding/json"
)

func (c *chatServer) registerClient(client *websocket.Client) {
	c.clients[client] = struct{}{}

	user, err := c.store.UserRepository.FindByLogin(client.Login)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	chats, err := c.store.ChatRepository.GetChats(user)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	var chatsData []models.SendChat
	for _, chat := range chats {
		chatData := models.SendChat{
			Id:       chat.Id,
			Users:    chat.Users,
			Messages: chat.Messages,
		}

		chatsData = append(chatsData, chatData)
	}

	sendMessage := models.SendMessage{
		Type: "chats",
		Data: chatsData,
	}

	sendMessageJson, err := json.Marshal(sendMessage)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	client.Send <- sendMessageJson
}

func (c *chatServer) unregisterClient(client *websocket.Client) {
	if _, ok := c.clients[client]; ok {
		delete(c.clients, client)
		close(client.Send)
	}
}

func (c *chatServer) receiveMessage(message *websocket.FromClientMessage) {
	user, err := c.store.UserRepository.FindByLogin(message.Client.Login)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	var receiveMessage models.ReceiveMessage
	err = json.Unmarshal(message.Message, &receiveMessage)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	switch receiveMessage.Type {
	case "new_message":
		var data models.ReceiveNewMessageData
		if err := json.Unmarshal(receiveMessage.Data, &data); err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		sqlMessage := &models.Message{
			UserLogin: user.Login,
			ChatId:    data.ChatId,
			Message:   data.Text,
		}

		err = c.store.ChatRepository.CreateMessage(user.Id, sqlMessage)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		c.broadcast("update_chat",
			models.SendChat{
				Id:    data.ChatId,
				Users: nil,
				Messages: []*models.Message{
					{
						Id:        sqlMessage.Id,
						UserLogin: sqlMessage.UserLogin,
						ChatId:    sqlMessage.ChatId,
						Message:   sqlMessage.Message,
					},
				},
			})
	case "create_chat":
		var data models.ReceiveCreateChat
		if err := json.Unmarshal(receiveMessage.Data, &data); err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		isCurentUserNotInclude := true

		for _, receiveUser := range data.Users {
			if receiveUser.Id == user.Id {
				isCurentUserNotInclude = false
			}
		}

		if isCurentUserNotInclude {
			data.Users = append(data.Users, user)
		}

		chat, err := c.store.ChatRepository.CreateChat(data.Users)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		c.broadcast("new_chat",
			models.SendChat{
				Id:       chat.Id,
				Users:    chat.Users,
				Messages: chat.Messages,
			})
	}
}

func (c *chatServer) sendMessage(message []byte) {
	var receiveMessage models.ReceiveMessage
	err := json.Unmarshal(message, &receiveMessage)
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}

	switch receiveMessage.Type {
	case "new_chat", "update_chat":
		var data models.SendChat
		if err := json.Unmarshal(receiveMessage.Data, &data); err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		users, err := c.store.ChatRepository.GetUsers(data.Id)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			return
		}

		for _, user := range users {
			for client := range c.clients {
				if client.Login == user.Login {
					select {
					case client.Send <- message:
					default:
						delete(c.clients, client)
					}
				}
			}
		}
	}
}

func (c *chatServer) broadcast(messageType string, messageData interface{}) {
	rawMsg, err := json.Marshal(models.SendMessage{
		Type: messageType,
		Data: messageData,
	})
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
		return
	}
	c.toDataBus <- rawMsg
}
