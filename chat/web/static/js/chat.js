const initialState = {
    ws: null,

    notification: "",

    chats: [],

    selected_chat: null,
    selected_users: [],

    message_to_send: "",
};

const app = new Vue({
    el: '#app',
    delimiters: ["<%", "%>"],
    data: initialState,
    created: function () {
        this.__connectToWS();
    },
    computed: {
        isChatsVisible: function () {
            return !(this.chats.length === 0)
        },
        isCreateChatVisible: function () {
            return !(this.selected_users.length === 0)
        }
    },
    methods: {
        __connectToWS() {
            this.ws = null;
            this.ws = new WebSocket(`ws://${window.location.host}/v1/ws`);
            this.__bind_websocket();
        },
        __bind_websocket() {
            this.ws.onopen = () => {
                this.notification = "Connection open";
            };

            this.ws.onclose = () => {
                this.notification = "Connection close";

                setTimeout(() => {
                    this.notification = "Try connection...";
                }, 2000);

                setTimeout(() => {
                    this.__connectToWS();
                }, 5000);
            };

            this.ws.onmessage = ev => {
                const message = JSON.parse(ev.data);
                console.log(message);
                const data = message.data;

                switch (message.type) {
                    case "chats":
                        this.__onMessageChats(data);
                        break;
                    case "new_chat":
                        this.__onMessageNewChat(data);
                        break;
                    case "update_chat":
                        this.__onMessageUpdateChat(data);
                        break;
                }
            }
        },
        __onMessageChats(chats) {
            for (let i = 0; i < chats.length; i++) {
                this.__onMessageNewChat(chats[i])
            }
        },
        __onMessageNewChat(chat) {
            let isChatNotInclude = true;

            for (let i = 0; i < this.chats.length; i++) {
                const iterChat = this.chats[i];

                if (iterChat.id === chat.id) {
                    isChatNotInclude = false
                }
            }

            if (isChatNotInclude) {
                if (this.selected_chat == null) {
                    this.selected_chat = chat
                }
                this.chats.push(chat)
            }
        },
        __onMessageUpdateChat(chat) {
            if (chat.users || chat.messages) {
                for (let i = 0; i < this.chats.length; i++) {
                    const iterChat = this.chats[i];

                    if (chat.id === iterChat.id) {
                        if (chat.users) {
                            for (let i = 0; i < chat.users.length; i++) {
                                this.__newUser(iterChat, chat.users[i])
                            }
                        }

                        if (chat.messages) {
                            for (let i = 0; i < chat.messages.length; i++) {
                                this.__newMessage(iterChat, chat.messages[i])
                            }
                        }
                    }
                }
            }
        },
        __newUser(chat, user) {
            if (chat.users) {
                let userNotInclude = true;

                for (let i = 0; i < chat.users.length; i++) {
                    const iterUser = chat.users[i];

                    if (iterUser.id === user.id) {
                        userNotInclude = false
                    }
                }

                if (userNotInclude) {
                    chat.users.push(user)
                }
            } else {
                chat.users = [user]
            }
        },
        __newMessage(chat, message) {
            if (chat.messages) {
                let messageNotInclude = true;

                for (let i = 0; i < chat.messages.length; i++) {
                    const iterMessage = chat.messages[i];

                    if (iterMessage.id === message.id) {
                        messageNotInclude = false
                    }
                }

                if (messageNotInclude) {
                    chat.messages.push(message)
                }
            } else {
                chat.messages = [message]
            }
        },
        __sendMessageToWS(message) {
            if (this.ws) {
                console.log(message);
                this.ws.send(JSON.stringify(message));
            }
        },
        createChatClicked() {
            if (this.selected_users && this.selected_users.length > 0) {
                const messageToSend = {
                    type: 'create_chat',
                    data: {
                        users: this.selected_users
                    }
                };
                this.__sendMessageToWS(messageToSend)
            }
        },
        sendMessageClicked() {
            if (this.selected_chat && this.message_to_send) {
                const messageToSend = {
                    type: 'new_message',
                    data: {
                        chat_id: this.selected_chat.id,
                        text: this.message_to_send
                    }
                };
                this.__sendMessageToWS(messageToSend)
            }
        },
        isSelectedChat(chat) {
            return chat === this.selected_chat;
        },
        chatSelected(chat) {
            if (chat !== this.selected_chat) {
                this.selected_chat = chat;
                this.selected_users = [];
            }
        },
        isSelectedUser(user) {
            return this.selected_users.includes(user)
        },
        userSelected(user) {
            if (this.selected_users.includes(user)) {
                const index = this.selected_users.indexOf(user);
                if (index > -1) {
                    this.selected_users.splice(index, 1)
                }
            } else {
                this.selected_users.push(user)
            }
        },
    }
});


// function testProto() {
//     // let AwesomeMessage = root.lookupType("awesomepackage.AwesomeMessage");
//     //
//     // let awesomeMessage = AwesomeMessage.create({some_field: "hello world"});
//     // console.log("myMessage", awesomeMessage);
//     //
//     // let buffer = AwesomeMessage.encode(myMessage).finish();
//     // console.log("buffer", Array.prototype.toString.call(buffer));
//     //
//     // let decoded = AwesomeMessage.decode(buffer);
//     // console.log("decoded", JSON.stringify(decoded));
//
//     function parseRoot(root) {
//         let AwesomeMessage = root.lookupType("awesomepackage.AwesomeMessage");
//         let payload = { awesome_field: "AwesomeString" };
//         let errMsg = AwesomeMessage.verify(payload);
//         if (errMsg)
//             throw Error(errMsg);
//
//         var message = AwesomeMessage.create(payload);
//         let buffer = AwesomeMessage.encode(message).finish();
//         message = AwesomeMessage.decode(buffer);
//         let object = AwesomeMessage.toObject(message, {
//             longs: String,
//             enums: String,
//             bytes: String,
//         });
//
//         console.log("buffer", buffer);
//         console.log("message", message);
//         console.log("object", object)
//     }
//
//     let proto = "syntax=\"proto3\";\
// package awesomepackage;\
// message AwesomeMessage {\
//   string awesome_field = 1;\
// }";
//     let root = protobuf.parse(proto, { keepCase: true }).root;
//     parseRoot(root)
//
//     // protobuf.load("mymessage.proto", function(err, root) {
//     //     if (err)
//     //         throw err;
//     //
//     //     parseRoot(root)
//     // });
// }