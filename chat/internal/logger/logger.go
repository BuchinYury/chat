package logger

import (
	"github.com/rs/zerolog"
	"os"
	"sync"
	"time"
)

var (
	logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.UnixDate}).With().Caller().Timestamp().
		Logger()
	mutex  = sync.RWMutex{}
)

func ConfigureLogger(loglevel string) error {
	mutex.Lock()
	defer mutex.Unlock()

	logLevel, err := zerolog.ParseLevel(loglevel)
	if err != nil {
		return err
	}
	logger = logger.Level(logLevel)

	logger.Info().Msg("Configure logger success")

	return nil
}

func GetLogger() *zerolog.Logger {
	mutex.RLock()
	defer mutex.RUnlock()

	return &logger
}

//func LogInfo(msg string) {
//	getLogger().Info().Msg(msg)
//}
//
//func LogWarn(msg string) {
//	getLogger().Warn().Msg(msg)
//}
//
//func LogError(msg string) {
//	getLogger().Warn().Msg(msg)
//}
//
//func LogFatal(msg string) {
//	getLogger().Fatal().Msg(msg)
//}
//
//func LogPanic(msg string) {
//	getLogger().Panic().Msg(msg)
//}
