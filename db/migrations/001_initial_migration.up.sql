CREATE DATABASE if not exists chat;
USE chat;

CREATE TABLE if not exists users
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE if not exists chats
(
    id  INT AUTO_INCREMENT PRIMARY KEY
);

INSERT INTO chats (id) values (null);

CREATE TABLE if not exists chat_users
(
    id      INT AUTO_INCREMENT PRIMARY KEY,
    chat_id INT NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT chat_users_chats_fk
        FOREIGN KEY (chat_id) REFERENCES chats (id),
    CONSTRAINT chat_users_users_fk
        FOREIGN KEY (user_id) REFERENCES users (id),
    UNIQUE (user_id, chat_id)
);

CREATE TABLE if not exists messages
(
    id       INT AUTO_INCREMENT PRIMARY KEY,
    users_id INT,
    chat_id  INT,
    message  TEXT NOT NULL,

    CONSTRAINT messages_users_fk
        FOREIGN KEY (users_id) REFERENCES users (Id),
    CONSTRAINT messages_chat_fk
        FOREIGN KEY (chat_id) REFERENCES chats (Id)
);
