package chatserver

import (
	"chat/internal/app/chat/models"
	"chat/internal/app/chat/store/databus"
	"chat/internal/app/chat/store/sqlstore"
	"chat/internal/app/chat/websocket"
	"chat/internal/logger"
	"chat/internal/middleware"
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"html/template"
	"io"
	"net/http"
	"os"
	"time"
)

type chatServer struct {
	config Config
	router *http.ServeMux

	clients    map[*websocket.Client]struct{}
	register   chan *websocket.Client
	receive    chan *websocket.FromClientMessage
	unregister chan *websocket.Client

	fromDataBus chan []byte
	toDataBus   chan []byte

	store *sqlstore.Store
}

func New(config Config, dataBus databus.DataBus, store *sqlstore.Store) (*chatServer, error) {
	fromDataBus := make(chan []byte)
	err := dataBus.SubscribeToChannel(config.DataBusChannel, fromDataBus)
	if err != nil {
		return nil, err
	}
	toDataBus := make(chan []byte)
	dataBus.PublishToChannel(config.DataBusChannel, toDataBus)

	chatServer := &chatServer{
		config: config,
		router: http.NewServeMux(),

		clients:    make(map[*websocket.Client]struct{}),
		register:   make(chan *websocket.Client),
		receive:    make(chan *websocket.FromClientMessage),
		unregister: make(chan *websocket.Client),

		fromDataBus: fromDataBus,
		toDataBus:   toDataBus,

		store: store,
	}

	chatServer.configureRouter()

	return chatServer, nil
}

func (c *chatServer) Run() error {
	c.runWSHandlers()
	return http.ListenAndServe(c.config.BindAddr, c.router)
}

func (c *chatServer) runWSHandlers() {
	go func() {
		for {
			select {
			case client := <-c.register:
				c.registerClient(client)
			case client := <-c.unregister:
				c.unregisterClient(client)
			case sendMessage := <-c.fromDataBus:
				c.sendMessage(sendMessage)
			case receiveMessage := <-c.receive:
				c.receiveMessage(receiveMessage)
			}
		}
	}()
}

func (c *chatServer) configureRouter() {
	rootHandler := middleware.ChainMiddleware(c.rootHandler,
		middleware.Log,
		middleware.PanicRecover,
		StaticFile,
		AuthenticateUser,
	)
	loginHandler := middleware.ChainMiddleware(c.loginHandler,
		middleware.Log,
		middleware.PanicRecover,
	)

	wsHandler := middleware.ChainMiddleware(c.wsHandler,
		middleware.PanicRecover,
		AuthenticateUser,
	)

	c.router.HandleFunc("/", rootHandler)
	c.router.HandleFunc("/login", loginHandler)
	c.router.HandleFunc("/v1/ws", wsHandler)
	c.router.HandleFunc("/whois", c.whoisHandler)
}

func (c *chatServer) whoisHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		err := http.StatusText(http.StatusMethodNotAllowed)
		logger.GetLogger().Error().Msg(err)
		http.Error(w, err, http.StatusMethodNotAllowed)
		return
	}

	_, err := io.WriteString(w, os.Getenv("WHOIS"))
	if err != nil {
		logger.GetLogger().Error().Msg(err.Error())
	}
}

func (c *chatServer) rootHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		err := http.StatusText(http.StatusMethodNotAllowed)
		logger.GetLogger().Error().Msg(err)
		http.Error(w, err, http.StatusMethodNotAllowed)
		return
	}

	if login, ok := r.Context().Value(CtxKeyUser).(string); ok {
		err := template.Must(template.ParseFiles(
			"web/templates/root.html",
			"web/templates/chat.html",
		)).Execute(w, models.ChatTemplateData{Login: login})
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		logger.GetLogger().Panic().Msg("CtxKeyUser value not string")
	}
}

func (c *chatServer) loginHandler(w http.ResponseWriter, r *http.Request) {
	executeTemplate := func(data models.LoginTemplateData) {
		err := template.Must(template.ParseFiles(
			"web/templates/root.html",
			"web/templates/login.html",
		)).Execute(w, data)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}

	if r.Method == http.MethodGet {
		executeTemplate(models.LoginTemplateData{})
		return
	}

	if r.Method == http.MethodPost {
		login := r.FormValue("login")

		if login == "" {
			http.Error(w, ErrLoginNotBeEmpty.Error(), http.StatusBadRequest)
			return
		}

		err := c.loginUser(login)
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			executeTemplate(models.LoginTemplateData{Error: ErrBadLogin.Error()})
			return
		}

		c := http.Cookie{
			Name:    "user",
			Value:   login,
			Expires: time.Now().Add(time.Duration(c.config.LoginCookieExpire) * time.Minute),
		}
		http.SetCookie(w, &c)

		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	err := http.StatusText(http.StatusMethodNotAllowed)
	logger.GetLogger().Error().Msg(err)
	http.Error(w, err, http.StatusMethodNotAllowed)
}

func (c *chatServer) loginUser(login string) error {
	_, err := c.store.UserRepository.FindByLogin(login)
	if err != nil {
		if err == sql.ErrNoRows {
			user := &models.User{
				Login: login,
			}

			if err := c.store.UserRepository.Create(user); err != nil {
				errMy, ok := err.(*mysql.MySQLError)
				if !ok || errMy.Number != 1062 {
					return err
				}
			}

			if err := c.store.ChatRepository.AddUser(c.config.CommonChatId, user); err != nil {
				return err
			}

			c.broadcast("update_chat",
				models.SendChat{
					Id: c.config.CommonChatId,
					Users: []*models.User{
						{
							Id:    user.Id,
							Login: user.Login,
						},
					},
					Messages: nil,
				})

			return nil
		}

		return err
	}

	return nil
}

func (c *chatServer) wsHandler(w http.ResponseWriter, r *http.Request) {
	if login, ok := r.Context().Value(CtxKeyUser).(string); ok {
		logger.GetLogger().Info().Msg("User " + login + " open ws connection")
		client, err := websocket.NewClient(w, r, login,
			websocket.ClientHandler{
				OnConnected:    c.register,
				OnDisconnected: c.unregister,
				OnMessage:      c.receive,
			})
		if err != nil {
			logger.GetLogger().Error().Msg(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		client.Run()
	} else {
		logger.GetLogger().Panic().Msg("CtxKeyUser value not string")
	}
}
