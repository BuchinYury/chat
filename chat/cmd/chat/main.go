package main

import (
	"chat/internal/app/chat/chatserver"
	"chat/internal/app/chat/store/databus"
	"chat/internal/app/chat/store/sqlstore"
	"chat/internal/logger"
	"flag"
	"fmt"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "configs/chat.json", "path to config file")
}

func main() {
	flag.Parse()
	config := parseConfig()
	fmt.Println(config)

	configureLogger(*config)

	redisStore, err := databus.New(config.RedisConfig.Addr, config.RedisConfig.Password, config.RedisConfig.Db)
	if err != nil {
		logger.GetLogger().Fatal().Msg(err.Error())
	}

	db, err := sqlstore.NewDB(config.DatabaseUrl)
	if err != nil {
		logger.GetLogger().Fatal().Msg(err.Error())
	}
	defer db.Close()

	sqlstr := sqlstore.New(db)

	err = runServer(*config, redisStore, sqlstr)
	if err != nil {
		logger.GetLogger().Fatal().Msg(err.Error())
	}
}

func parseConfig() *chatserver.Config {
	config, err := chatserver.FromFile(configPath)
	if err != nil {
		logger.GetLogger().Fatal().Msg(err.Error())
	}
	return config
}

func configureLogger(config chatserver.Config) {
	err := logger.ConfigureLogger(config.LogLevel)
	if err != nil {
		logger.GetLogger().Fatal().Msg(err.Error())
	}
}

func runServer(config chatserver.Config, redis *databus.Redis, store *sqlstore.Store) error {
	chtserver, err := chatserver.New(config, redis, store)
	if err != nil {
		return err
	}
	err = chtserver.Run()
	if err != nil {
		return err
	}
	return nil
}
